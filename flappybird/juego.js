var cvs = document.getElementById("canvas");
var ctx = cvs.getContext("2d");

// Carga img

var pajarito = new Image();
var bg = new Image();
var fg = new Image();
var tuboNorte = new Image();
var tuboSur = new Image();

pajarito.src = "img/pajarito.png";
bg.src = "img/bg.png";
fg.src = "img/fg.png";
tuboNorte.src = "img/tuboNorte.png";
tuboSur.src = "img/tuboSur.png";


// Variables

var brecha = 100;
var constant;

var bX = 10;
var bY = 150;

var gravedad = 1.75;

var puntaje = 0;

// Archivos de audio

var volar = new Audio();
var puntuar = new Audio();

volar.src = "audio/volar.mp3";
puntuar.src = "audio/puntaje.mp3";

// Captura del teclado

document.addEventListener("keydown",moveUp);

function moveUp(){
    bY -= 40 ;
    volar.play();
}

// Coordenada de los tubos

var pipe = [];

pipe[0] = {
    x : cvs.width,
    y : 0
};

// Función dibujar

function draw(){
    
    ctx.drawImage(bg,0,0);
    for(var i = 0; i < pipe.length; i++){
        
        constant = tuboNorte.height+brecha;
        ctx.drawImage(tuboNorte,pipe[i].x,pipe[i].y);
        ctx.drawImage(tuboSur,pipe[i].x,pipe[i].y+constant);
             
        pipe[i].x--;
        
        if( pipe[i].x == 125 ){
            pipe.push({
                x : cvs.width,
                y : Math.floor(Math.random()*tuboNorte.height)-tuboNorte.height
            }); 
        }

        // Si se detecta colisión se recarga la página
        if( bX + pajarito.width >= pipe[i].x && bX <= pipe[i].x + tuboNorte.width && (bY <= pipe[i].y + tuboNorte.height || bY+pajarito.height >= pipe[i].y+constant) || bY + pajarito.height >=  cvs.height - fg.height){
            location.reload();
        }
        if(pipe[i].x == 5){
            puntaje++;
            puntuar.play();
        }
        
        
    }

    ctx.drawImage(fg,0,cvs.height - fg.height);
    ctx.drawImage(pajarito,bX,bY);
    
    bY += gravedad;
    
    ctx.fillStyle = "#000";
    ctx.font = "20px Bitter";
    ctx.fillText("Puntaje : "+puntaje,10,cvs.height-20);
    
    requestAnimationFrame(draw);
    
}

draw();
























