var micanvas = document.getElementById("canvas");	//Canvas de mi html
var contexto = micanvas.getContext("2d");        	//Contexto de mi canvas


//Imagenes
var fondo = new Image();
var personaje = new Image();
var enemigo = new Image();

fondo.src = "img/bg.jpg";					//imagen de fondo
personaje.src = "img/ironman_left.png";		//imagen de mi personaje izquierda
enemigo.src = "img/thanos.png";				//imagen de mi enemigo


//Audio
var vuelo = new Audio();
var musica = new Audio();

vuelo.src = "media/vuelo.mp3";				//audio del vuelo
musica.src = "media/portales.mp3";			//audio de portales avengers endgame xd

//Variables
var personaje_x = 400;	//ubicacion de iron man en X
var personaje_y = 150;	//ubicacion de iron man en Y

var enemigo_x = 400;	//ubicacion de thanos en X
var enemigo_y = 270;	//ubicacion de thanos en Y

var personaje_dir = 0 	// 0: no see mueve, -1: izquierda, 1: derecha;

//Listener de eventos
document.addEventListener("keydown",girarPersonaje);


//Funcion para girar personaje
function girarPersonaje(){
	//Izquierda
	if(event.keyCode == 37){						//tecla
		personaje_dir = -1;							//-1: izquierda
		personaje.src = "img/ironman_left.png"; 	//imagen de mi personaje izquierda
		vuelo.pause();								//Pausar melodia
		vuelo.currentTime = 0;						//indicar donde empieza a reproducir
		vuelo.play();								//Reproducir melodia
	}
	//Derecha
	if(event.keyCode == 39){						//tecla
		personaje_dir = +1;							//1: derecha
		personaje.src = "img/ironman_right.png";	//imagen de mi personaje derecha
		vuelo.pause();								//Pausar melodia
		vuelo.currentTime = 0;						//indicar donde empieza a reproducir
		vuelo.play();								//Reproducir melodia
	}
}

function dibujar(){
	contexto.drawImage(fondo,0,0,1023,491);							//Dibujando mi fondo

	//super condionales para evitar que iron man salga del cuadro
	if(personaje_dir == -1){
		if(personaje_x > 0){
			personaje_x = personaje_x - 10;	//mover al iron man
		}else{
			personaje_x = 0;				//cuadrando al iron man xd
			personaje_dir = 0;
		}
	}
	if(personaje_dir == +1){
		if(personaje_x < 900){
			personaje_x = personaje_x + 10;	//mover al iron man
		}else{
			personaje_x = 900;				//cuadrando al iron man xd
			personaje_dir = 0;
		}
	}

	contexto.drawImage(personaje,personaje_x,personaje_y,123,93);	//Dibujando mi personaje
	contexto.drawImage(enemigo,enemigo_x,enemigo_y,144,220);		//DIbujando mi enemigo

	requestAnimationFrame(dibujar);
	//Este método indica al navegador que se debería ejecutar la función entre paréntesis. 
	//El navegador llama a la función cuando está listo para actualizar la ventana, 
	//sincronizando la animación con la ventana del navegador y la pantalla del ordenador.
}


musica.play();	//Reproducir melodia
dibujar();		//Llamando a mi funcion dibujar